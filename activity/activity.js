// Result of querying for users with letter 's' in their first name or 'd' in their last name.

db.users.find( {$or: [ { firstName: {$regex: 's', $options: '$i'} }, { lastName: {$regex: 'd', $options: '$i'} } ] });


// Result of querying for users who are from the HR department and their age is greater than  or equal to 70.

db.users.find( {$or: [ { department: "HR" }, { age: { $gte: 70} } ] });


// Result of querying for users with the letter 'e' in their first name and has an age of less than or equal to 30.

db.users.find({ $and: [ { firstName: {$regex: 'e'} }, { age : { $lte: 30 } }] });


